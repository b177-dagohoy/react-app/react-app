import React, {useState, useEffect, useContext} from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Login(){

    const { user, setUser } = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    console.log(email);
    console.log(password);

    function authenticate(e){
        e.preventDefault();

        // Set the email of the user in the local storage
        // Syntax:
        //  localStorage.setItem('propertyName', value);

        localStorage.setItem('email', email);

        setUser({
            email: localStorage.getItem('email')
        })

        setEmail('');
        setPassword('');

        alert('You are now logged in!')
    }

    useEffect(() => {
        if(email !== '' && password !== '' ){
            setIsActive(true);
        }
        else{
            setIsActive(false)
        }
    }, [email, password]);

    return (
        (user.email !== null) ?
            <Navigate to="/courses" />
        :    
        <Form onSubmit={(e) => authenticate(e)}>
            <h1>Login</h1>
            <Form.Group className="mb-3" controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

            { isActive ?
                <Button variant="success" type="submit" id="submitBtn">
                    Submit
                </Button>
              :
                <Button variant="secondary" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }
            
        </Form>
    )
}
